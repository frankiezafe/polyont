﻿import fontforge
import xml.etree.ElementTree as ET
import os
import copy
import sys

'''
ABCDEFGHIJKLMNOPQRSTUVWXYZ
abcdefghijklmnopqrstuvwxyz
!"#$%&'()*+,-./:;<=>?@[\]_{|}~¡©
'''

# fontforge project
FF_FILE = '../polyont_thin.sfd'

# svg generation
ONLY_ONE_LETTER = True
GLYPHS_SVG = 'thin.svg'

# characters width
char_width = {
	'aeiou"\'': 375,
	'!#$%&()*+,-_.?\/|0I:;<=>[]{}~¡© ' : 500,
	'bcdfghjklmnpqrstvwxyz': 625,
	'1234567890AEOUYL' : 750,
	'BCDFGHJKMNPQRSTVWXZ@' : 1000
}

def generate_svgs():
	
	export_folder = os.path.splitext(GLYPHS_SVG)[0]
	tree = ET.parse(GLYPHS_SVG)
	root = tree.getroot()
	listoflayers=[]
	for g in root.findall('{http://www.w3.org/2000/svg}g'):
		name = g.get('{http://www.inkscape.org/namespaces/inkscape}label')
		listoflayers.append(name)
	
	if not os.path.exists(export_folder):
		os.makedirs(export_folder)
	
	try:
		listoflayers.remove('background')
	except ValueError:
		pass
		#print("No background")
	
	for counter in range(len(listoflayers)):
		lname = listoflayers[counter]
		if len( lname ) == 1:
			print( 'character', lname )
			lname = str(ord( lname ))
		elif ONLY_ONE_LETTER:
			continue
		james=listoflayers[:]
		temp_tree = copy.deepcopy(tree)
		del james[counter]
		temp_root = temp_tree.getroot()
		for g in temp_root.findall('{http://www.w3.org/2000/svg}g'):
			name = g.get('{http://www.inkscape.org/namespaces/inkscape}label')
			if name in james:
				temp_root.remove(g)
			else:
				style = g.get('style')
				if type(style) is str:
					style = style.replace( 'display:none', 'display:inline' )
					g.set('style', style)
		p = os.path.join( export_folder, lname +'.svg' )
		temp_tree.write( p )

def load_svgs():
	
	export_folder = os.path.splitext(GLYPHS_SVG)[0]
	svgs = []
	for file in os.listdir(export_folder):
		if file.endswith(".svg"):
			try:
				i = int( os.path.splitext(file)[0] )
				char = chr( i )
				svgs.append( [ os.path.join( export_folder, file ), i, char ] )
			except:
				pass
	
	for svg in svgs:
		print( 'loading', svg[0] )
		c = font.createMappedChar( svg[1] )
		c.importOutlines( svg[0] )

def resize():
	for k in char_width:
		for c in k:
			try:
				font[ord(c)].width = char_width[ k ]
			except:
				print( 'failed to resize:', c, ord(c) )

font = fontforge.open( FF_FILE )
print( '##############\nloading ' + FF_FILE + '\n##############' )
generate_svgs()
load_svgs()
resize()
