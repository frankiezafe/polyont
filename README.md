# polyont

![](https://gitlab.com/frankiezafe/polyont/raw/master/resources/20191112_medium_thin.png)

typography tool research, based on inkscape and fontforge

- sfd files are fontforge project
- svg/ contains all characters for variants
- python/fontforge_import.py is a script that load the svg file with all characters into fontforge

## naming convention

in inkscape, each characters must be in a 1000x1000 viewport and each layer must be named with the character contained in it

once all required chars are created, go to python folder

## python

to use python script

- launch fontforge,
- go to **File > Execute** Script
- copy/paste the python, adapt path FF_FILE & GLYPHS_SVG
- execute script
- verify that everything is ok

and go to **File > Generate Fonts**

## refs

- https://inkscape.org
- https://fontforge.org
- https://fontforge.org/python.html (python API for fontforge)
- https://github.com/frankiezafe/layer-to-svg
